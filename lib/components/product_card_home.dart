import 'package:flutter/material.dart';
import 'package:single_vendor_ecom/components/default_button.dart';
import 'package:single_vendor_ecom/components/image_view.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/pages/product_details_page.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class ProductCardHome extends StatefulWidget {
  int index, itemCount;
  Direction direction;
  var data;
  ProductCardHome(
      {Key? key,
      required this.index,
      required this.itemCount,
      required this.data,
      required this.direction})
      : super(key: key);

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCardHome> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.only(
          left: (widget.index == 0 && Direction.horizontal == widget.direction)
              ? 5
              : 0,
          right: (widget.index == (widget.itemCount - 1) &&
                  Direction.horizontal == widget.direction)
              ? 5
              : 0,
        ),
        child: Card(
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Container(
              width: 150,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Container(
                      height: 125,
                      margin: EdgeInsets.only(
                        left: 5,
                        right: 5,
                        top: 5,
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: ImageView(
                            url: kBaseUrl + widget.data['default_image_url']),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.data['product_name'],
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(
                          height: 1,
                        ),
                        Text(
                          widget.data['variant_name'],
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 12,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              '₹${widget.data['sale_price']}',
                              maxLines: 1,
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: kPriceColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                            if (widget.data['regular_price'] ==
                                widget.data['sale_price']) ...[
                              SizedBox(width: 2),
                              Text(
                                '₹${widget.data['regular_price']}',
                                maxLines: 1,
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  decoration: TextDecoration.lineThrough,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 11,
                                ),
                              ),
                            ]
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onTap: () {
        NavigationUtls.navigate(
            ProductDetailsPage(
              productId: widget.data['id'],
            ),
            context);
      },
    );
  }
}
