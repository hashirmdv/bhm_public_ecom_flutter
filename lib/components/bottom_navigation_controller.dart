import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/pages/category_page.dart';
import 'package:single_vendor_ecom/pages/dashboard_page.dart';
import 'package:single_vendor_ecom/pages/home_page.dart';
import 'package:single_vendor_ecom/pages/order_list_page.dart';
import 'package:single_vendor_ecom/pages/wishlist_page.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class BottomNavigationController extends StatefulWidget {
  PageState pageState;
  BottomNavigationController({required this.pageState});
  @override
  _BottomNavigationControllerState createState() =>
      _BottomNavigationControllerState();
}

class _BottomNavigationControllerState
    extends State<BottomNavigationController> {
  int _selectedIndex = 0;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<BottomNavigationBarItem> items = [
    BottomNavigationBarItem(icon: Icon(Icons.home), label: ""),
    BottomNavigationBarItem(icon: Icon(Icons.dashboard), label: ""),
    BottomNavigationBarItem(icon: Icon(Icons.delivery_dining), label: ""),
    BottomNavigationBarItem(icon: Icon(Icons.favorite), label: ""),
    BottomNavigationBarItem(icon: Icon(Icons.person), label: ""),
  ];

  @override
  void initState() {
    _selectedIndex = _getIndexFromPageState(widget.pageState);
    super.initState();
  }

  _getIndexFromPageState(PageState pageState) {
    if (pageState == PageState.home) {
      return 0;
    } else if (pageState == PageState.categories) {
      return 1;
    } else if (pageState == PageState.orders) {
      return 2;
    } else if (pageState == PageState.wishlist) {
      return 3;
    } else if (pageState == PageState.dashboard) {
      return 4;
    } else {
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: [
        HomePage(),
        CategoryPage(),
        OrderListPage(),
        WishListPage(),
        DashboardPage(),
      ].elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        selectedFontSize: 0,
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: kPrimaryColor,
        elevation: 60,
        unselectedItemColor: kSecondaryColor,
        currentIndex: _selectedIndex,
        items: items,
        onTap: _changePage,
      ),
    );
  }

  void _changePage(int value) {
    if (mounted)
      setState(() {
        _selectedIndex = value;
      });
  }
}
