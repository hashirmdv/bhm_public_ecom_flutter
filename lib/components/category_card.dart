import 'package:flutter/material.dart';
import 'package:single_vendor_ecom/components/image_view.dart';
import 'package:single_vendor_ecom/models/category_model.dart';
import 'package:single_vendor_ecom/pages/product_list_page.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class CategoryCard extends StatefulWidget {
  int index, itemCount;
  CategoryModel categoryModel;
  Direction direction;
  CategoryCard(
      {Key? key,
      required this.index,
      required this.itemCount,
      required this.categoryModel,
      required this.direction})
      : super(key: key);

  @override
  _CategoryCardState createState() => _CategoryCardState();
}

class _CategoryCardState extends State<CategoryCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.only(
          left: (widget.index == 0 && Direction.horizontal == widget.direction)
              ? 5
              : 0,
          right: (widget.index == (widget.itemCount - 1) &&
                  Direction.horizontal == widget.direction)
              ? 5
              : 0,
        ),
        child: Card(
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: Container(
              width: 90,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 60,
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 5, right: 5),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: ImageView(
                          url: widget.categoryModel.imageUrl,
                        )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    widget.categoryModel.name,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      onTap: () {
        NavigationUtls.navigate(
            ProductListPage(
              categoryId: widget.categoryModel.id,
              categoryName: widget.categoryModel.name,
            ),
            context);
      },
    );
  }
}
