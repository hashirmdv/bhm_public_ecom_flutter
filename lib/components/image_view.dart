import 'package:flutter/material.dart';

class ImageView extends StatelessWidget {
  String url;

  ImageView({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
      url,
      fit: BoxFit.fill,
      errorBuilder: (context, error, stackTrace) {
        return Center(
          child: Icon(
            Icons.error,
          ),
        );
      },
    );
  }
}

// class ImageView111 extends StatefulWidget {
//   String url;
//   ImageView111({Key? key, required this.url}) : super(key: key);

//   @override
//   _ImageViewState createState() => _ImageViewState();
// }

// class _ImageViewState extends State<ImageView> {
//   @override
//   Widget build(BuildContext context) {
//     return Image.network(
//       widget.url,
//       errorBuilder: (context, error, stackTrace) {
//         return Center(
//           child: Icon(
//             Icons.error,
//           ),
//         );
//       },
//     );
//   }
// }
