class DatabaseHelper {
  static String dbName = "bhm_public.db";
  static int dbVersion = 1;

//=================== table cart ===================
  static String tableCarts = "carts";

  static String cartId = "cart_id";
  static String productId = "product_id";
  static String productName = "product_name";
  static String categoryId = "category_id";
  static String categoryName = "category_name";
  static String variantId = "variant_id";
  static String variantName = "variant_name";
  static String productDescription = "description";
  static String productSalePrice = "sale_price";
  static String productRegularPrice = "regular_price";
  static String imageURL = "image_url";
  static String quantity = "quantity";

  static String createCartsTable = "CREATE TABLE " +
      tableCarts +
      "(" +
      cartId +
      " INTEGER PRIMARY KEY AUTOINCREMENT," +
      productId +
      " INTEGER," +
      productName +
      " TEXT," +
      productDescription +
      " TEXT," +
      categoryId +
      " INTEGER," +
      categoryName +
      " TEXT," +
      variantId +
      " integer," +
      variantName +
      " TEXT," +
      productSalePrice +
      " REAL," +
      productRegularPrice +
      " REAL," +
      quantity +
      " REAL," +
      imageURL +
      " TEXT" +
      ")";
}
