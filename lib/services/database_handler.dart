import 'package:single_vendor_ecom/models/product_model.dart';
import 'package:single_vendor_ecom/services/database_helper.dart';
import 'package:single_vendor_ecom/utils/app_logger.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHandler {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();
    return openDatabase(
      join(path, DatabaseHelper.dbName),
      version: DatabaseHelper.dbVersion,
      onCreate: (database, version) async {
        print("DatabaseHandler: onCreate");
        await database.execute(DatabaseHelper.createCartsTable);
      },
      onUpgrade: (db, oldVersion, newVersion) async {
        print("DatabaseHandler: onUpgrade");
        print("OnUpgrade from " +
            oldVersion.toString() +
            " to " +
            newVersion.toString());

        int upgradeTo = oldVersion + 1;
        while (upgradeTo <= newVersion) {
          switch (upgradeTo) {
            case 2:
              {
                // await db.execute(DBServices.createNotificationsTable);
                break;
              }
            case 3:
              {
                // await db.execute(DBServices.createKeyValueTable);
                break;
              }
          }
          upgradeTo++;
        }
      },
      onDowngrade: (db, oldVersion, newVersion) {
        print("DatabaseHandler: onDowngrade");

        if (oldVersion < newVersion) {
          int upgradeTo = oldVersion + 1;
          while (upgradeTo <= newVersion) {
            switch (upgradeTo) {
              // Excute commands here
            }
            upgradeTo++;
          }
        }
      },
    );
  }

  Future<int> insertToCart(ProductModel product) async {
    AppLogger.log("insertToCart");

    int result = 0;
    final Database db = await initializeDB();

    result = await db.insert(DatabaseHelper.tableCarts, product.toCartMap());
    return result;
  }

  Future<List<ProductModel>> getAllCarts() async {
    AppLogger.log("getAllCarts");

    final Database db = await initializeDB();
    final List<Map<String, Object?>> queryResult =
        await db.query(DatabaseHelper.tableCarts);

    return queryResult.map((e) => ProductModel.fromCartMap(e)).toList();
  }

  getProductOnCart(int productId, int variantId) async {
    AppLogger.log("getProductOnCart");

    final Database db = await initializeDB();
    var res = await db.query(
      DatabaseHelper.tableCarts,
      where: "product_id = ? and variant_id = ?",
      whereArgs: [productId, variantId],
    );

    return res.isNotEmpty ? ProductModel.fromCartMap(res.first) : null;
  }

  Future<void> deleteProductFromCart(int productId, int variantId) async {
    AppLogger.log("deleteProductFromCart");

    final db = await initializeDB();

    await db.delete(
      DatabaseHelper.tableCarts,
      where: "product_id = ? and variant_id = ?",
      whereArgs: [productId, variantId],
    );
  }

  Future<void> updateProductQtyOnCart(
      int productId, int variantId, int qty) async {
    AppLogger.log("updateProductQtyOnCart");

    final db = await initializeDB();
    await db.update(
      DatabaseHelper.tableCarts,
      {'quantity': qty},
      where: "product_id = ? and variant_id = ?",
      whereArgs: [productId, variantId],
    );
  }

  Future<void> emptyCart() async {
    AppLogger.log("emptyCart");

    final db = await initializeDB();

    await db.delete(
      DatabaseHelper.tableCarts,
    );
  }
}
