import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:single_vendor_ecom/cons/app_const.dart';

class RestService {
  static String baseUrl = kApiBaseUrl;

  static makeFormPostRequestAuth(String url, Map params, Function success,
      Function failure, Function loader,
      {String? token}) async {
    print("METHOD => POST");
    print("URL => " + baseUrl + url);
    print("INPUT => " + jsonEncode(params));
    try {
      loader(true);
      http.Response res = await http.post(
        Uri.parse(baseUrl + url),
        headers: {
          "Accept": "application/json",
          'Authorization': 'Token $token',
        },
        body: params,
      );
      print("STATUS CODE => " + res.statusCode.toString());
      print('OUTPUT => ' + res.body);
      loader(false);
      if (res.statusCode == 200 || res.statusCode == 201) {
        Map data = json.decode(res.body);
        success(data);
      } else {
        Map data = json.decode(res.body);
        print('Request failed with status: ${res.statusCode}.');
        failure(data['error'].toString());
      }
    } catch (error) {
      failure("Internal error occurred");
      print("Internal error occurred");
      print(error);
    }
  }

  static makeGetRequestAuth(
      String url, Function success, Function failure, Function loader,
      {String? token}) async {
    print("METHOD => GET");
    print("URL => " + baseUrl + url);
    try {
      loader(true);
      print("Token $token");
      http.Response res = await http.get(
        Uri.parse(baseUrl + url),
        headers: {
          "Accept": "application/json",
          'Authorization': 'Token $token',
        },
      );

      print("STATUS CODE => " + res.statusCode.toString());
      print('OUTPUT => ' + res.body);
      loader(false);
      if (res.statusCode == 200 || res.statusCode == 201) {
        Map data = json.decode(res.body);
        success(data);
      } else {
        Map data = json.decode(res.body);
        print('Request failed with status: ${res.statusCode}.');
        failure(data['error']);
      }
    } catch (error) {
      failure("Internal error occurred");
      print("Internal error occurred");
      print(error);
    }
  }

  static makePostRequestOpen(String url, Map params, Function success,
      Function failure, Function loader) async {
    print("METHOD => POST");
    print("URL => " + baseUrl + url);
    print("INPUT => " + jsonEncode(params));
    try {
      loader(true);
      http.Response res = await http.post(
        Uri.parse(baseUrl + url),
        headers: {
          "Accept": "application/json",
        },
        body: params,
      );
      print("STATUS CODE => " + res.statusCode.toString());
      print('OUTPUT => ' + res.body);
      loader(false);
      if (res.statusCode == 200 || res.statusCode == 201) {
        Map data = json.decode(res.body);
        if (data['code'] == 0) {
          success(data);
        } else {
          failure(data['message'].toString());
        }
      } else {
        Map data = json.decode(res.body);
        print('Request failed with status: ${res.statusCode}.');
        failure(data['message'].toString());
      }
    } on SocketException catch (error) {
      failure(
          "Cannot connect to the server. please check your internet connection and try again.");
      print(
          "Cannot connect to the server. please check your internet connection and try again.");
    } catch (error) {
      failure("Internal error occurred");
      print("Internal error occurred");
      print(error);
    }
  }

  static makeGetRequestOpen(
      String url, Function success, Function failure, Function loader) async {
    print("METHOD => GET");
    print("URL => " + baseUrl + url);
    try {
      loader(true);
      http.Response res = await http.get(
        Uri.parse(baseUrl + url),
        headers: {
          "Accept": "application/json",
        },
      );
      print("STATUS CODE => " + res.statusCode.toString());
      print('OUTPUT => ' + res.body);
      loader(false);
      if (res.statusCode == 200 || res.statusCode == 201) {
        Map data = json.decode(res.body);
        if (data['code'] == 0) {
          success(data);
        } else {
          failure(data['message'].toString());
        }
      } else {
        Map data = json.decode(res.body);
        print('Request failed with status: ${res.statusCode}.');
        failure(data['message'].toString());
      }
    } on SocketException catch (error) {
      failure(
          "Cannot connect to the server. please check your internet connection and try again.");
      print(
          "Cannot connect to the server. please check your internet connection and try again.");
    } catch (error) {
      failure("Internal error occurred");
      print("Internal error occurred");
      print(error);
    }
  }
}
