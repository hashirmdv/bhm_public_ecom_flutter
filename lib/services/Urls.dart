class Urls {
  static String check_user_exist = "check-user-and-send-otp";
  static String verify_otp = "verify-otp";
  static String resend_otp = "resend-otp";
  static String register = "register";

  static String check_pincode = "check-pincode";
  static String get_home_categories = "get-home-categories";
  static String get_home_products = "get-home-products";
  static String get_product_details = "get-product-details";
  static String get_related_products = "get-related-products";

  static String get_categories = "get-categories";
  static String get_products_by_category = "get-products-by-category";
  static String search_products = "search-product";
}
