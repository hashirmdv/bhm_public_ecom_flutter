import 'package:flutter/material.dart';

const Color kYellow = Color(0xffFDC054);

const kPrimaryColor = Color(0xFF65c048);
const kSecondaryColor = Color(0xFF757575);

const kTextColor = Color(0xFF757575);
const kBackgroundColor = Color(0xFFefefef);
const kHintColor = Color(0xFFdddddd);
const kPriceColor = Color(0xFF4b9172);
const kWhiteColor = Color(0xFFFFFFFF);
const kDarkColor = Color(0xFF303030);
const kLightColor = Color(0xFF808080);
const kTransparent = Colors.transparent;

const kGreyColor = Color(0xFF808080);
