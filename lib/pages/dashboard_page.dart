import 'package:flutter/material.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/pages/cart_page.dart';
import 'package:single_vendor_ecom/pages/search_page.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class DashboardPage extends StatefulWidget {
  DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: Text(kAppName),
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () async {
              NavigationUtls.navigate(SearchPage(), context);
            },
          ),
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () async {
              NavigationUtls.navigate(CartPage(), context);
            },
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [],
      ),
    );
  }
}
