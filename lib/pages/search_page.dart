import 'package:flutter/material.dart';
import 'package:single_vendor_ecom/components/product_card.dart';
import 'package:single_vendor_ecom/models/product_model.dart';
import 'package:single_vendor_ecom/services/Urls.dart';
import 'package:single_vendor_ecom/services/rest_service.dart';
import 'package:single_vendor_ecom/values/colors.dart';

class SearchPage extends StatefulWidget {
  SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List<ProductModel>? _productList;
  bool _isLoading = false;
  String? _errorMessage;

  @override
  void initState() {
    super.initState();
  }

  fetchData(String query) {
    String url = Urls.search_products + "?name=" + query.toString();

    RestService.makeGetRequestOpen(url, (Map response) {
      print("On success");

      List dataList = response['products'];

      ProductModel product;
      List<ProductModel> _tempList = [];
      for (var data in dataList) {
        product = new ProductModel(
          id: data['id'],
          name: data['product_name'],
          regularPrice: double.parse(data['regular_price']),
          salePrice: double.parse(data['sale_price']),
          imageUrl: data['default_image_url'],
          categoryId: data['category_id'],
          categoryName: data['category_name'],
          variantId: data['variant_id'],
          variantName: data['variant_name'],
          productDescription: data['description'],
          quantity: 0,
        );

        _tempList.add(product);
      }

      if (mounted)
        setState(() {
          _productList = _tempList;
        });
    }, (String error) {
      print("On Error");
      print(error);
      setState(() {
        _productList = null;
      });
    }, (bool loader) {
      print("On Loader");
      setState(() {
        _isLoading = loader;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _isLoading ? Center(child: CircularProgressIndicator()) : _body(),
    );
  }

  _appBar() {
    return AppBar(
      backgroundColor: kPrimaryColor,
      elevation: 0,
      title: TextField(
        autofocus: true,
        style: TextStyle(color: Colors.white),
        cursorColor: Colors.grey,
        decoration: const InputDecoration(
          border: InputBorder.none,
          hintText: 'Search here...',
          hintStyle: TextStyle(
            color: Colors.white,
          ),
        ),
        onChanged: (value) {
          fetchData(value);
        },
      ),
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 5, right: 5),
            child: _products(),
          ),
        ],
      ),
    );
  }

  _products() {
    var maxWidth = 200.0;
    var width = MediaQuery.of(context).size.width;
    var columns = (width ~/ maxWidth) + 1;
    var columnWidth = width / columns;
    //160 is the height of one grid item
    var aspectRatio = columnWidth / 250;

    return _productList != null
        ? GridView.builder(
            itemCount: _productList!.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: maxWidth,
              childAspectRatio: aspectRatio,
            ),
            itemBuilder: (BuildContext context, int index) {
              return ProductCard(
                productModel: _productList![index],
              );
            },
          )
        : Padding(
            padding: const EdgeInsets.only(top: 50.0),
            child: Center(child: Text(_errorMessage ?? "")),
          );
  }
}
