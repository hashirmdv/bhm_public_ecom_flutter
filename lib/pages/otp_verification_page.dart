import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:single_vendor_ecom/components/bottom_navigation_controller.dart';
import 'package:single_vendor_ecom/components/default_button.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/pages/sign_up_page.dart';
import 'package:single_vendor_ecom/services/urls.dart';
import 'package:single_vendor_ecom/services/rest_service.dart';
import 'package:single_vendor_ecom/utils/app_logger.dart';
import 'package:single_vendor_ecom/utils/keyboard_utils.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/utils/size_utils.dart';
import 'package:single_vendor_ecom/utils/snackbar_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class OtpVerificationPage extends StatefulWidget {
  String mobileNo;
  bool isExist;
  OtpVerificationPage({Key? key, required this.mobileNo, required this.isExist})
      : super(key: key);

  @override
  _OtpVerificationState createState() => _OtpVerificationState();
}

class _OtpVerificationState extends State<OtpVerificationPage> {
  TextEditingController _otpController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final _otpLength = 6;
  bool isLoading = false;
  bool isMasterLoading = false;

  String? errorMessage;
  late String mobile;
  late bool isExits;
  late String otp;

  @override
  void initState() {
    super.initState();
    mobile = widget.mobileNo;
    isExits = widget.isExist;
  }

  _appBar() {
    return AppBar(
      elevation: 0,
      iconTheme: IconThemeData(
        color: Colors.grey[700], //change your color here
      ),
      title: Text(
        "OTP Verification",
        style: TextStyle(
          color: Colors.grey[700],
        ),
      ),
      backgroundColor: kBackgroundColor,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      backgroundColor: kBackgroundColor,
      body: ModalProgressHUD(
        inAsyncCall: isMasterLoading,
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(20),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 25,
                  ),
                  // SizedBox(
                  //   height: 50,
                  // ),
                  // Container(
                  //   height: 100,
                  //   child: Image.asset('assets/images/logo.png'),
                  // ),
                  SizedBox(height: 25),
                  Text(
                    "We have sent a verification code to",
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 5),
                  Text(
                    mobile,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 25),
                  if (errorMessage != null) ...[
                    Text(
                      errorMessage ?? "",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.red,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        buildOTPFormField(),
                        SizedBox(height: 15),
                        SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: ElevatedButton(
                            style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              primary: Colors.white,
                              backgroundColor: kPrimaryColor,
                            ),
                            onPressed: () {
                              if (!isLoading) {
                                if (_formKey.currentState!.validate()) {
                                  _formKey.currentState!.save();

                                  makeRequestToVerifyOtp();
                                }
                              } else {
                                print("Already loading...");
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5, right: 5),
                              child: isLoading
                                  ? SizedBox(
                                      height: 20,
                                      width: 20,
                                      child: CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation(
                                            Colors.white),
                                        backgroundColor: Colors.blue,
                                        strokeWidth: 3,
                                      ),
                                    )
                                  : Text(
                                      "VERIFY",
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Don't receive the code? "),
                      GestureDetector(
                        onTap: () {
                          makeRequestToResendOtp();
                        },
                        child: Text(
                          "Resend Now",
                          style: TextStyle(
                            color: kPrimaryColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  PinCodeTextField buildOTPFormField() {
    return PinCodeTextField(
      appContext: context,
      length: _otpLength,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.underline,
        selectedFillColor: kPrimaryColor,
        selectedColor: kPrimaryColor,
        activeColor: kPrimaryColor,
        activeFillColor: kPrimaryColor,
        inactiveColor: kPrimaryColor,
        inactiveFillColor: kPrimaryColor,
        borderRadius: BorderRadius.circular(5),
        fieldHeight: 30,
        fieldWidth: 40,
      ),
      textStyle: TextStyle(fontSize: 16),
      keyboardType: TextInputType.number,
      animationDuration: Duration(milliseconds: 300),
      controller: _otpController,
      validator: (value) {
        if (value!.isEmpty) {
          return "Enter OTP";
        } else if (value.length != _otpLength) {
          return "Enter valid otp";
        }
        return null;
      },
      onSaved: (newValue) => otp = newValue!,
      onCompleted: (pin) {},
      onChanged: (value) {
        setState(() {
          errorMessage = null;
        });
      },
      beforeTextPaste: (text) {
        return true;
      },
    );
  }

  makeRequestToVerifyOtp() async {
    setState(() {
      errorMessage = null;
    });

    Map params = {
      "phone_number": mobile,
      "otp": otp,
    };

    RestService.makePostRequestOpen(Urls.verify_otp, params, (Map response) {
      print("On success");
      if (isExits) {
        AppLogger.log("AAA 111");
        saveUserData(response['user_details'], response['access_token']);
      } else {
        AppLogger.log("AAA 222");
        NavigationUtls.navigateAndFinish(
            SignUpPage(
              mobile: mobile,
            ),
            context);
      }
    }, (String error) {
      print("On Error");
      print(error);
      setState(() {
        errorMessage = error;
      });
    }, (bool loader) {
      print("On Loader");
      setState(() {
        isLoading = loader;
      });
    });
  }

  saveUserData(Map<String, dynamic> _map, String token) async {
    print("Save user data");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(kSharedPrefUserDetailsKey, json.encode(_map));
    prefs.setString(kSharedPrefUserTokenKey, token);

    NavigationUtls.navigateAndFinish(
        BottomNavigationController(
          pageState: PageState.home,
        ),
        context);
  }

  makeRequestToResendOtp() async {
    Map params = {
      "phone_number": mobile,
    };

    RestService.makePostRequestOpen(Urls.resend_otp, params, (Map response) {
      print("On success");
      showSnackBar(context, "OTP successfully resend to " + mobile.toString());
    }, (String error) {
      print("On Error");
      print(error);
      setState(() {
        errorMessage = error;
      });
    }, (bool loader) {
      print("On Loader");
      setState(() {
        isMasterLoading = loader;
      });
    });
  }
}
