import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:single_vendor_ecom/components/bottom_navigation_controller.dart';
import 'package:single_vendor_ecom/components/image_view.dart';
import 'package:single_vendor_ecom/components/product_card.dart';
import 'package:single_vendor_ecom/components/product_card_home.dart';
import 'package:single_vendor_ecom/cons/api_keys.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/models/category_model.dart';
import 'package:single_vendor_ecom/models/home_item_model.dart';
import 'package:single_vendor_ecom/pages/cart_page.dart';
import 'package:single_vendor_ecom/pages/product_list_page.dart';
import 'package:single_vendor_ecom/pages/search_page.dart';
import 'package:single_vendor_ecom/services/urls.dart';
import 'package:single_vendor_ecom/services/database_handler.dart';
import 'package:single_vendor_ecom/services/rest_service.dart';
import 'package:single_vendor_ecom/utils/app_logger.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/utils/user_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late DatabaseHandler _dbHandler;

  List _imageSliders = [];
  String? _locationName;
  bool _isLoading = true, _isDeliveryAvailable = false;
  bool _isLoadingCategory = true, _isLoadingProducts = true;

  List<CategoryModel>? _categoryList;
  List<HomeItemModel>? _homeItemList;

  @override
  void initState() {
    _dbHandler = DatabaseHandler();
    _dbHandler.initializeDB().whenComplete(() {
      print("DB Initilization completed");
    });

    _checkLocationExist();

    _imageSliders = [
      ImageView(
        url:
            'http://homedelo.com/app/uploads/homeSlider/08_09_2021_12_11_47_4149576_id_61385b2b22123.jpeg',
      ),
      ImageView(
        url:
            'http://homedelo.com/app/uploads/homeSlider/28_06_2021_11_24_53_8723448_id_60d9642ddac1a.jpeg',
      ),
      ImageView(
        url:
            'http://homedelo.com/app/uploads/homeSlider/05_01_2021_22_53_24_5265224_id_5ff4a08c0c684.png',
      ),
      ImageView(
        url:
            'http://homedelo.com/app/uploads/homeSlider/24_12_2020_14_33_30_5865905_id_5fe459625729c.png',
      ),
      ImageView(
        url:
            'http://homedelo.com/app/uploads/homeSlider/17_08_2021_13_04_20_5613838_id_611b667cd09cc.jpeg',
      ),
    ];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: _appBar(),
      body: _setupBody(),
    );
  }

  _setupBody() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    } else {
      if (_isDeliveryAvailable) {
        return _body();
      } else {
        return _deliveryNotAvailable();
      }
    }
  }

  _deliveryNotAvailable() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/images/delivery_bike.png",
            height: 100,
          ),
          SizedBox(
            height: 10,
          ),
          Text("Delivery not available here."),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
            onPressed: () {
              _openLocationPicker();
            },
            child: Text("Change Location"),
          )
        ],
      ),
    );
  }

  _openLocationPicker() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PlacePicker(
          apiKey: kGoogleMapKey, // Put YOUR OWN KEY here.
          onPlacePicked: (result) {
            print(result.formattedAddress);

            _saveLocation(
                result.geometry!.location.lat, result.geometry!.location.lng);

            Navigator.of(context).pop();
          },
          initialPosition: kInitialPosition,
          useCurrentLocation: true,
        ),
      ),
    );
  }

  _appBar() {
    return AppBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        ),
      ),
      title: GestureDetector(
        onTap: () {
          _openLocationPicker();
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              kAppName,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 2.0,
            ),
            Row(
              children: [
                Icon(
                  Icons.my_location,
                  size: 14.0,
                  color: Colors.white,
                ),
                SizedBox(
                  width: 3.0,
                ),
                Text(
                  _locationName ?? "--",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      backgroundColor: kPrimaryColor,
      actions: [
        IconButton(
          icon: Icon(Icons.notifications),
          onPressed: () {
            print("Clicked");
          },
        ),
        IconButton(
          icon: Icon(Icons.shopping_cart),
          onPressed: () {
            NavigationUtls.navigate(CartPage(), context);
          },
        ),
      ],
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: Container(
          margin: EdgeInsets.only(
            top: 10.0,
            bottom: 15.0,
            right: 20.0,
            left: 20.0,
          ),
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 6.0,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 45.0,
          child: InkWell(
            onTap: () {
              NavigationUtls.navigate(SearchPage(), context);
            },
            child: TextField(
              enabled: false,
              style: TextStyle(
                color: Colors.black,
              ),
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(top: 10.0, left: 10.0),
                prefixIcon: Icon(
                  Icons.search,
                ),
                hintText: "Search Products",
                hintStyle: TextStyle(
                  color: Colors.grey.shade400,
                  fontSize: 15.0,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Column(
        children: [
          // SizedBox(height: 10),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 10.0),
          //   child: Container(
          //     decoration: BoxDecoration(
          //       color: Colors.white,
          //       borderRadius: BorderRadius.circular(10),
          //     ),
          //     child: TextField(
          //       enabled: false,
          //       decoration: InputDecoration(
          //           contentPadding: EdgeInsets.only(top: 15.0),
          //           border: InputBorder.none,
          //           focusedBorder: InputBorder.none,
          //           enabledBorder: InputBorder.none,
          //           hintText: "Search product",
          //           prefixIcon: Icon(Icons.search)),
          //     ),
          //   ),
          // ),
          SizedBox(height: 10),

          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Row(
              children: [
                Text(
                  "Categories",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    print("Clicked");
                    NavigationUtls.navigate(
                        BottomNavigationController(
                          pageState: PageState.categories,
                        ),
                        context);
                  },
                  child: Text(
                    "Show all",
                    style: TextStyle(
                      color: kPrimaryColor,
                      fontSize: 14,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _categories(),
          ),

          SizedBox(
            height: 5,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Row(
              children: [
                Text(
                  "Special For You",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: SizedBox(
              height: 200.0,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: Carousel(
                  autoplay: true,
                  borderRadius: true,
                  showIndicator: true,
                  images: _imageSliders,
                  dotSize: 7.0,
                  dotSpacing: 25.0,
                  indicatorBgPadding: 10.0,
                  dotBgColor: Colors.purple.withOpacity(0),
                ),
              ),
            ),
          ),
          // Container(
          //   height: 180,
          //   margin: EdgeInsets.only(
          //     left: 10,
          //     right: 10,
          //   ),
          //   width: double.infinity,
          //   child: ClipRRect(
          //     borderRadius: BorderRadius.circular(10.0),
          //     child: ImageView(
          //       url: 'https://hashroid.com/dailyshopi/banner.jpg',
          //     ),
          //   ),
          // ),
          _products()
        ],
      ),
    );
  }

  _categories() {
    var maxWidth = 175.0;
    var width = MediaQuery.of(context).size.width;
    var columns = (width ~/ maxWidth) + 1;
    var columnWidth = width / columns;
    //160 is the height of one grid item
    var aspectRatio = columnWidth / 140;

    return GridView.builder(
      itemCount: _categoryList == null ? 0 : _categoryList!.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: maxWidth,
        childAspectRatio: aspectRatio,
      ),
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          child: Card(
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 85,
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 5, right: 5),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: ImageView(
                        url: kBaseUrl + _categoryList![index].imageUrl,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    _categoryList![index].name,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
          ),
          onTap: () {
            NavigationUtls.navigate(
                ProductListPage(
                  categoryId: _categoryList![index].id,
                  categoryName: _categoryList![index].name,
                ),
                context);
          },
        );
      },
    );
  }

  _products() {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: _homeItemList == null ? 0 : _homeItemList!.length,
      itemBuilder: (context, index) {
        var data = _homeItemList![index];
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 10,
                right: 10,
              ),
              child: Row(
                children: [
                  Text(
                    data.name,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () {
                      NavigationUtls.navigate(
                          ProductListPage(
                            categoryId: data.id,
                            categoryName: data.name,
                          ),
                          context);
                    },
                    child: Text(
                      "Show all",
                      style: TextStyle(
                        color: kPrimaryColor,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if (data.products.length > 0)
              Container(
                height: 215,
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: data.products.length,
                  itemBuilder: (context, pos) {
                    return ProductCardHome(
                      index: pos,
                      data: data.products[pos],
                      itemCount: data.products.length,
                      direction: Direction.horizontal,
                    );
                  },
                ),
              ),
          ],
        );
      },
    );
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  Future<Position> _getCurrentLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    Position pos = await Geolocator.getCurrentPosition();
    print("Location");
    print("Latitude: ${pos.latitude}");
    print("Longitude: ${pos.longitude}");

    _saveLocation(pos.latitude, pos.longitude);

    return pos;
  }

  _saveLocation(latitude, longitude) async {
    try {
      List<Placemark> placemarks =
          await placemarkFromCoordinates(latitude, longitude);
      print(placemarks[0]);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(kSharedPrefLocationKey, json.encode(placemarks[0]));

      _checkLocationExist();
    } catch (err) {}
  }

  _checkLocationExist() async {
    String? location = await UserUtils.getLocation();
    if (location == null) {
      AppLogger.log("Location Not Exist");
      _getCurrentLocation();
    } else {
      Map _loc = json.decode(location);
      print(_loc['postalCode']);

      setState(() {
        _isLoading = false;
        _locationName = _loc['subLocality'] + ", " + _loc['locality'];
      });

      AppLogger.log("Location Exist");

      _makeRequestToCheckDeliveryAvailable(_loc['postalCode']);
    }
  }

  _makeRequestToCheckDeliveryAvailable(postalCode) {
    Map params = {
      "pincode": postalCode,
    };

    RestService.makePostRequestOpen(Urls.check_pincode, params, (Map response) {
      print("On success");

      setState(() {
        _isDeliveryAvailable = response['is_delivery_available'];
      });

      if (response['is_delivery_available']) {
        fetchCategories();
        fetchProducts();
        //Make request to load items
      } else {
        // Show delivery not available screen
      }
    }, (String error) {
      print("On Error");
      print(error);
    }, (bool loader) {
      print("On Loader");
      setState(() {
        _isLoading = loader;
      });
    });
  }

  fetchCategories() {
    RestService.makeGetRequestOpen(Urls.get_home_categories, (Map response) {
      print("On success");

      List dataList = response['categories'];

      CategoryModel categoryModel;
      List<CategoryModel> _tempList = [];
      for (var data in dataList) {
        categoryModel = new CategoryModel(
            id: data['id'], name: data['name'], imageUrl: data['thumbnail']);

        _tempList.add(categoryModel);
      }

      if (mounted)
        setState(() {
          _categoryList = _tempList;
        });
    }, (String error) {
      print("On Error");
      print(error);
    }, (bool loader) {
      print("On Loader");
      setState(() {
        _isLoading = loader;
      });
    });
  }

  fetchProducts() {
    RestService.makeGetRequestOpen(Urls.get_home_products, (Map response) {
      print("On success");

      List dataList = response['items'];

      HomeItemModel homeItemModel;
      List<HomeItemModel> _tempList = [];
      for (var data in dataList) {
        homeItemModel = new HomeItemModel();
        homeItemModel.id = data['id'];
        homeItemModel.name = data['name'];
        homeItemModel.imageUrl = data['thumbnail'];
        homeItemModel.products = data['products'];

        _tempList.add(homeItemModel);
      }

      if (mounted)
        setState(() {
          _homeItemList = _tempList;
        });
    }, (String error) {
      print("On Error");
      print(error);
    }, (bool loader) {
      print("On Loader");
      setState(() {
        _isLoading = loader;
      });
    });
  }
}
