import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:single_vendor_ecom/components/bottom_navigation_controller.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/pages/home_page.dart';
import 'package:single_vendor_ecom/pages/sign_in_page.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/utils/size_utils.dart';
import 'package:single_vendor_ecom/utils/user_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    init();
    super.initState();
  }

  init() async {
    String? profile = await UserUtils.getProfile();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isSkipped = prefs.getBool(kSharedPrefLoginSkippedKey) ?? false;

    Future.delayed(Duration(seconds: 3), () {
      NavigationUtls.navigateAndFinish(
          profile != null || isSkipped
              ? BottomNavigationController(
                  pageState: PageState.home,
                )
              : SignInPage(),
          context);
    });
  }

  @override
  Widget build(BuildContext context) {
    // You have to call it on your starting screen
    SizeUtils().init(context);
    return Scaffold(
      backgroundColor: kBackgroundColor,
      resizeToAvoidBottomInset: false,
      body: _body(),
    );
  }

  _body() {
    return SafeArea(
      child: Container(
        child: Center(
          child: Container(
            height: 200,
            child: Image.asset('assets/images/logo.png'),
          ),
        ),
      ),
    );
  }
}
