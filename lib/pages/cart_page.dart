import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:single_vendor_ecom/components/default_button.dart';
import 'package:single_vendor_ecom/components/image_view.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/models/product_model.dart';
import 'package:single_vendor_ecom/pages/checkout_page.dart';
import 'package:single_vendor_ecom/pages/order_success_page.dart';
import 'package:single_vendor_ecom/pages/product_details_page.dart';
import 'package:single_vendor_ecom/services/database_handler.dart';
import 'package:single_vendor_ecom/utils/app_logger.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/utils/size_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';

class CartPage extends StatefulWidget {
  CartPage({Key? key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  late DatabaseHandler _dbHandler;

  int _itemsCountOnCart = 0;
  double _itemsTotalAmountOnCart = 0;

  @override
  void initState() {
    _dbHandler = DatabaseHandler();
    super.initState();

    updateBottomBarDatas();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(context),
      body: _body1(),
      bottomNavigationBar: _bottomBar(),
    );
  }

  _body1() {
    return FutureBuilder(
      future: _dbHandler.getAllCarts(),
      builder: (context, AsyncSnapshot<List<ProductModel>> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data!.length > 0) {
            return Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(10)),
              child: ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  print(snapshot.data![index].toString());
                  return Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 0),
                        child: ListTile(
                          leading: Container(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: ImageView(
                                  url: snapshot.data![index].imageUrl),
                            ),
                          ),
                          onTap: () {
                            NavigationUtls.navigate(
                                ProductDetailsPage(
                                  productId: snapshot.data![index].id,
                                ),
                                context);
                          },
                          title: Text(snapshot.data![index].name +
                              " (" +
                              snapshot.data![index].variantName +
                              ")"),
                          subtitle: Text(
                            '₹${snapshot.data![index].salePrice}',
                          ),
                          trailing: Column(
                            children: [
                              // GestureDetector(
                              //   onTap: () {
                              //     showAlertDialog(
                              //         context,
                              //         snapshot.data![index].id,
                              //         snapshot.data![index].variantId);
                              //   },
                              //   child: Icon(
                              //     Icons.delete,
                              //     color: Colors.red,
                              //   ),
                              // ),
                              GestureDetector(
                                onTap: () async {
                                  int qty =
                                      snapshot.data![index].quantity.round() +
                                          1;
                                  await _dbHandler.updateProductQtyOnCart(
                                    snapshot.data![index].id,
                                    snapshot.data![index].variantId,
                                    qty,
                                  );
                                  updateBottomBarDatas();
                                },
                                child: Container(
                                  width: 30,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                  ),
                                  child: Center(
                                    child: Text("+"),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                  top: 1,
                                  bottom: 1,
                                ),
                                child: Text(
                                  snapshot.data![index].quantity
                                      .round()
                                      .toString(),
                                ),
                              ),
                              GestureDetector(
                                onTap: () async {
                                  if (snapshot.data![index].quantity > 1) {
                                    int qty =
                                        snapshot.data![index].quantity.round() -
                                            1;
                                    await _dbHandler.updateProductQtyOnCart(
                                      snapshot.data![index].id,
                                      snapshot.data![index].variantId,
                                      qty,
                                    );

                                    updateBottomBarDatas();
                                  } else {
                                    showAlertDialog(
                                      context,
                                      snapshot.data![index].id,
                                      snapshot.data![index].variantId,
                                    );
                                  }
                                },
                                child: Container(
                                  width: 30,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                  ),
                                  child: Center(
                                    child: Text("-"),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Divider(),
                    ],
                  );
                },
              ),
            );
          } else {
            return Center(child: Text('No items in cart.'));
          }
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  showAlertDialog(BuildContext context, int productId, int variantId) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        "Delete Item",
        style: TextStyle(
          color: Colors.red,
        ),
      ),
      onPressed: () async {
        await _dbHandler.deleteProductFromCart(productId, variantId);
        Navigator.of(context, rootNavigator: true).pop('dialog');
        updateBottomBarDatas();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text("Are you sure want to delete item from cart?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _bottomBar() {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: getProportionateScreenWidth(15),
        horizontal: getProportionateScreenWidth(30),
      ),
      // height: 174,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          )
        ],
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Row(
            //   children: [
            //     Container(
            //       padding: EdgeInsets.all(10),
            //       height: getProportionateScreenWidth(40),
            //       width: getProportionateScreenWidth(40),
            //       decoration: BoxDecoration(
            //         color: Color(0xFFF5F6F9),
            //         borderRadius: BorderRadius.circular(10),
            //       ),
            //       child: SvgPicture.asset("assets/icons/receipt.svg"),
            //     ),
            //     Spacer(),
            //     Text("Add voucher code"),
            //     const SizedBox(width: 10),
            //     Icon(
            //       Icons.arrow_forward_ios,
            //       size: 12,
            //     )
            //   ],
            // ),
            SizedBox(height: getProportionateScreenHeight(1)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text.rich(
                  TextSpan(
                    text: "Total:\n",
                    children: [
                      TextSpan(
                        text: "₹$_itemsTotalAmountOnCart",
                        style: TextStyle(fontSize: 16, color: Colors.black),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: getProportionateScreenWidth(190),
                  child: DefaultButton(
                    text: "Checkout",
                    press: () {
                      NavigationUtls.navigate(CheckoutPage(), context);
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _appBar(BuildContext context) {
    return AppBar(
      backgroundColor: kPrimaryColor,
      elevation: 0,
      title: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Your Cart",
          ),
          Text(
            "$_itemsCountOnCart items",
            style: TextStyle(color: kHintColor, fontSize: 14.0),
          ),
        ],
      ),
    );
  }

  updateBottomBarDatas() async {
    List<ProductModel> list = await _dbHandler.getAllCarts();
    double amount = 0;
    for (var item in list) {
      amount = amount + (item.quantity * item.salePrice);
    }

    setState(() {
      _itemsCountOnCart = list.length;
      _itemsTotalAmountOnCart = amount;
    });
  }
}
