import 'package:flutter/material.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/pages/cart_page.dart';
import 'package:single_vendor_ecom/pages/search_page.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class WishListPage extends StatefulWidget {
  WishListPage({Key? key}) : super(key: key);

  @override
  _WishListPageState createState() => _WishListPageState();
}

class _WishListPageState extends State<WishListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: kPrimaryColor,
        title: Text("Wishlist"),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () async {
              NavigationUtls.navigate(SearchPage(), context);
            },
          ),
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () async {
              NavigationUtls.navigate(CartPage(), context);
            },
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [],
      ),
    );
  }
}
