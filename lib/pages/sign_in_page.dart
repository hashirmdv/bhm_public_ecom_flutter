import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:single_vendor_ecom/components/bottom_navigation_controller.dart';
import 'package:single_vendor_ecom/components/default_button.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/cons/string.dart';
import 'package:single_vendor_ecom/pages/otp_verification_page.dart';
import 'package:single_vendor_ecom/services/urls.dart';
import 'package:single_vendor_ecom/services/rest_service.dart';
import 'package:single_vendor_ecom/utils/keyboard_utils.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/utils/size_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class SignInPage extends StatefulWidget {
  SignInPage({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignInPage> {
  final _formKey = GlobalKey<FormState>();
  late String mobile;
  bool isLoading = false;
  String? errorMessage;

  @override
  void initState() {
    _getCurrentLocation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: _appBar(),
      backgroundColor: kBackgroundColor,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 25,
                ),
                SizedBox(
                  height: 50,
                ),
                Container(
                  height: 150,
                  child: Image.asset('assets/images/logo.png'),
                ),
                SizedBox(height: 25),
                Text(
                  "Welcome Back",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: getProportionateScreenWidth(28),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  "Sign in with your mobile to get all features",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
                if (errorMessage != null) ...[
                  Text(
                    errorMessage ?? "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.red,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      buildMobileFormField(),
                      SizedBox(height: 15),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: ElevatedButton(
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            primary: Colors.white,
                            backgroundColor: kPrimaryColor,
                          ),
                          onPressed: () {
                            if (!isLoading) {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();

                                makeRequestToCheckUser();
                              }
                            } else {
                              print("Already loading...");
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5, right: 5),
                            child: isLoading
                                ? SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          AlwaysStoppedAnimation(Colors.white),
                                      backgroundColor: Colors.blue,
                                      strokeWidth: 3,
                                    ),
                                  )
                                : Text(
                                    "CONTINUE",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        skip();
                      },
                      child: Text(
                        "Skip",
                        style: TextStyle(
                          fontSize: 16,
                          color: kPrimaryColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  skip() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(kSharedPrefLoginSkippedKey, true);

    NavigationUtls.navigateAndFinish(
        BottomNavigationController(
          pageState: PageState.home,
        ),
        context);
  }

  TextFormField buildMobileFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => mobile = newValue!,
      validator: (value) {
        if (value!.isEmpty) {
          return kMobileNullError;
        } else if (!(value.length == 10)) {
          return kInvalidMobileError;
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Mobile No.",
        hintText: "Enter your mobile number",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.phone_iphone,
          color: kGreyColor,
        ),
      ),
    );
  }

  makeRequestToCheckUser() async {
    setState(() {
      errorMessage = null;
    });

    Map params = {
      "phone_number": mobile,
    };

    RestService.makePostRequestOpen(Urls.check_user_exist, params,
        (Map response) {
      print("On success");
      NavigationUtls.navigate(
          OtpVerificationPage(
            isExist: response['is_exist'],
            mobileNo: mobile,
          ),
          context);
    }, (String error) {
      print("On Error");
      print(error);
      setState(() {
        errorMessage = error;
      });
    }, (bool loader) {
      print("On Loader");
      setState(() {
        isLoading = loader;
      });
    });
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  Future<Position> _getCurrentLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    Position pos = await Geolocator.getCurrentPosition();
    print("Location");
    print("Latitude: ${pos.latitude}");
    print("Longitude: ${pos.longitude}");
    return pos;
  }
}
