import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:single_vendor_ecom/components/default_button.dart';
import 'package:single_vendor_ecom/pages/order_success_page.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';

class CheckoutPage extends StatefulWidget {
  CheckoutPage({Key? key}) : super(key: key);

  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(context),
      backgroundColor: kWhiteColor,
      body: SingleChildScrollView(
        padding:
            EdgeInsets.only(left: 20.0, right: 20.0, top: 40.0, bottom: 10.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Subtotal"),
                Text("Rs. 500"),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("Delivery fee"),
                Text("Rs. 10"),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Total",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "Rs. 510",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
                color: Colors.grey.shade200,
                padding: EdgeInsets.all(8.0),
                width: double.infinity,
                child: Text("Delivery Address".toUpperCase())),
            Column(
              children: <Widget>[
                RadioListTile(
                  selected: true,
                  value: 'Kuruniyan House',
                  groupValue: 'Kuruniyan House',
                  title: Text('Kuruniyan House'),
                  onChanged: (dynamic value) {},
                ),
                RadioListTile(
                  selected: false,
                  value: "New Address",
                  groupValue: 'Kuruniyan House',
                  title: Text("Choose new delivery address"),
                  onChanged: (dynamic value) {},
                ),
                Container(
                    color: Colors.grey.shade200,
                    padding: EdgeInsets.all(8.0),
                    width: double.infinity,
                    child: Text("Contact Number".toUpperCase())),
                RadioListTile(
                  selected: true,
                  value: '9961357429',
                  groupValue: '9961354229',
                  title: Text('9961357429'),
                  onChanged: (dynamic value) {},
                ),
                RadioListTile(
                  selected: false,
                  value: "New Phone",
                  groupValue: '5976236426',
                  title: Text("Choose new contact number"),
                  onChanged: (dynamic value) {},
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
                color: Colors.grey.shade200,
                padding: EdgeInsets.all(8.0),
                width: double.infinity,
                child: Text("Payment Option".toUpperCase())),
            RadioListTile(
              groupValue: true,
              value: true,
              title: Text("Cash on Delivery"),
              onChanged: (dynamic value) {},
            ),
            SizedBox(height: 30.0),
            Container(
              width: double.infinity,
              child: DefaultButton(
                text: "Confirm Order",
                press: () {
                  NavigationUtls.navigate(OrderSuccessPage(), context);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  _appBar(BuildContext context) {
    return AppBar(
      backgroundColor: kPrimaryColor,
      elevation: 0,
      title: Text(
        "Confirm Order",
      ),
    );
  }
}
