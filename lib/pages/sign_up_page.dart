import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:single_vendor_ecom/components/bottom_navigation_controller.dart';
import 'package:single_vendor_ecom/components/default_button.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/cons/string.dart';
import 'package:single_vendor_ecom/pages/home_page.dart';
import 'package:single_vendor_ecom/pages/otp_verification_page.dart';
import 'package:single_vendor_ecom/services/urls.dart';
import 'package:single_vendor_ecom/services/rest_service.dart';
import 'package:single_vendor_ecom/utils/keyboard_utils.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/utils/size_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class SignUpPage extends StatefulWidget {
  String mobile;
  SignUpPage({Key? key, required this.mobile}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();
  late String mobile;
  late String name, email;
  String? errorMessage;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    mobile = widget.mobile;
  }

  _appBar() {
    return AppBar(
      elevation: 0,
      iconTheme: IconThemeData(
        color: Colors.grey[700], //change your color here
      ),
      title: Text(
        "Personal Details",
        style: TextStyle(
          color: Colors.grey[700],
        ),
      ),
      backgroundColor: kBackgroundColor,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      backgroundColor: kBackgroundColor,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                // SizedBox(
                //   height: 10,
                // ),
                // Container(
                //   height: 100,
                //   child: Image.asset('assets/images/logo.png'),
                // ),
                SizedBox(height: 25),
                Text(
                  "Enter your personal details to get all features",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
                if (errorMessage != null) ...[
                  Text(
                    errorMessage ?? "",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.red,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      buildNameFormField(),
                      SizedBox(
                        height: 5,
                      ),
                      buildEmailFormField(),
                      SizedBox(height: 15),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: ElevatedButton(
                          style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            primary: Colors.white,
                            backgroundColor: kPrimaryColor,
                          ),
                          onPressed: () {
                            if (!isLoading) {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();

                                makeRequestToRegister();
                              }
                            } else {
                              print("Already loading...");
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5, right: 5),
                            child: isLoading
                                ? SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          AlwaysStoppedAnimation(Colors.white),
                                      backgroundColor: Colors.blue,
                                      strokeWidth: 3,
                                    ),
                                  )
                                : Text(
                                    "COMPLETE",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  TextFormField buildNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.name,
      onSaved: (newValue) => name = newValue!,
      validator: (value) {
        if (value!.isEmpty) {
          return "Enter your name";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Name",
        hintText: "Enter your name",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.person,
          color: kGreyColor,
        ),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => email = newValue!,
      validator: (value) {
        if (value!.isEmpty) {
          return kEmailNullError;
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          return kInvalidEmailError;
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Email",
        hintText: "Enter your email",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(
          Icons.mail,
          color: kGreyColor,
        ),
      ),
    );
  }

  makeRequestToRegister() async {
    setState(() {
      errorMessage = null;
    });

    Map params = {
      "phone": mobile,
      "name": name,
      "email": email,
    };

    RestService.makePostRequestOpen(Urls.register, params, (Map response) {
      print("On success");
      saveUserData(response['user_details'], response['access_token']);
    }, (String error) {
      print("On Error");
      print(error);
      setState(() {
        errorMessage = error;
      });
    }, (bool loader) {
      print("On Loader");
      setState(() {
        isLoading = loader;
      });
    });
  }

  saveUserData(Map<String, dynamic> _map, String token) async {
    print("Save user data");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(kSharedPrefUserDetailsKey, json.encode(_map));
    prefs.setString(kSharedPrefUserTokenKey, token);

    NavigationUtls.navigateAndFinish(
        BottomNavigationController(
          pageState: PageState.home,
        ),
        context);
  }
}
