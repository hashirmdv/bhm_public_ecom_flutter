import 'package:flutter/material.dart';
import 'package:single_vendor_ecom/components/image_view.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/models/category_model.dart';
import 'package:single_vendor_ecom/pages/cart_page.dart';
import 'package:single_vendor_ecom/pages/product_list_page.dart';
import 'package:single_vendor_ecom/pages/search_page.dart';
import 'package:single_vendor_ecom/services/Urls.dart';
import 'package:single_vendor_ecom/services/rest_service.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class CategoryPage extends StatefulWidget {
  CategoryPage({Key? key}) : super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  List<CategoryModel>? _categoryList;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();

    fetchData();
  }

  fetchData() {
    RestService.makeGetRequestOpen(Urls.get_categories, (Map response) {
      print("On success");

      List dataList = response['categories'];

      var _tempList =
          dataList.map((data) => new CategoryModel.fromJson(data)).toList();

      if (mounted)
        setState(() {
          _categoryList = _tempList;
        });
    }, (String error) {
      print("On Error");
      print(error);
    }, (bool loader) {
      print("On Loader");
      setState(() {
        _isLoading = loader;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _isLoading ? Center(child: CircularProgressIndicator()) : _body(),
    );
  }

  _appBar() {
    return AppBar(
      backgroundColor: kPrimaryColor,
      elevation: 0,
      title: Text("Categories"),
      automaticallyImplyLeading: false,
      actions: [
        IconButton(
          icon: Icon(Icons.search),
          onPressed: () async {
            NavigationUtls.navigate(SearchPage(), context);
          },
        ),
        IconButton(
          icon: Icon(Icons.shopping_cart),
          onPressed: () async {
            NavigationUtls.navigate(CartPage(), context);
          },
        ),
      ],
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: _categories(),
          ),
        ],
      ),
    );
  }

  _categories() {
    var maxWidth = 175.0;
    var width = MediaQuery.of(context).size.width;
    var columns = (width ~/ maxWidth) + 1;
    var columnWidth = width / columns;
    //160 is the height of one grid item
    var aspectRatio = columnWidth / 140;

    return GridView.builder(
      itemCount: _categoryList != null ? _categoryList!.length : 0,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: maxWidth,
        childAspectRatio: aspectRatio,
      ),
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          child: Card(
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 85,
                    width: double.infinity,
                    margin: EdgeInsets.only(left: 5, right: 5),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: ImageView(
                            url: kBaseUrl + _categoryList![index].imageUrl)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    _categoryList![index].name,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
          ),
          onTap: () {
            NavigationUtls.navigate(
                ProductListPage(
                  categoryId: _categoryList![index].id,
                  categoryName: _categoryList![index].name,
                ),
                context);
          },
        );
      },
    );
  }
}
//  return jsonResponse.map((data) => new Data.fromJson(data)).toList();