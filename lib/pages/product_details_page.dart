import 'package:flutter/material.dart';
import 'package:single_vendor_ecom/components/default_button.dart';
import 'package:single_vendor_ecom/components/image_view.dart';
import 'package:single_vendor_ecom/components/product_card.dart';
import 'package:single_vendor_ecom/components/top_rounded_container.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/models/product_model.dart';
import 'package:single_vendor_ecom/pages/cart_page.dart';
import 'package:single_vendor_ecom/pages/checkout_page.dart';
import 'package:single_vendor_ecom/services/urls.dart';
import 'package:single_vendor_ecom/services/database_handler.dart';
import 'package:single_vendor_ecom/services/rest_service.dart';
import 'package:single_vendor_ecom/utils/app_logger.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';
import 'package:single_vendor_ecom/utils/size_utils.dart';
import 'package:single_vendor_ecom/utils/snackbar_utils.dart';
import 'package:single_vendor_ecom/values/colors.dart';
import 'package:single_vendor_ecom/values/enums.dart';

class ProductDetailsPage extends StatefulWidget {
  int productId;
  ProductDetailsPage({Key? key, required this.productId}) : super(key: key);

  @override
  _ProductDetailsPageState createState() => _ProductDetailsPageState();
}

class _ProductDetailsPageState extends State<ProductDetailsPage> {
  late DatabaseHandler _dbHandler;

  bool _isLiked = false;
  int _selectedImageIndex = 0;
  int _selectedVariantIndex = 0;
  bool _isFirstLoadVariant = true;

  late int _productId;
  List? _variants, _images;
  bool _isLoading = true;
  late String _productName, _description, _imageUrl;
  late double _regularPrice, _salePrice;

  late String _defualtImageUrl;
  late int _categoryId;
  late String _categoryName;
  late int _variantId;
  late String _variantName;

  late ProductModel? _productOnCart;

  int _itemsCountOnCart = 0;
  double _itemsTotalAmountOnCart = 0;
  int _itemCount = 0;
  List<ProductModel>? _relatedProdcuts;

  @override
  void initState() {
    super.initState();
    _productId = widget.productId;
    _dbHandler = DatabaseHandler();

    updateBottomBarDatas();
    fetchProductDetails();
    fetchRelatedProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _isLoading ? Center(child: CircularProgressIndicator()) : _body(),
      bottomNavigationBar: _bottomBar(),
    );
  }

  _appBar() {
    return AppBar(
      backgroundColor: kPrimaryColor,
      elevation: 0,
      title: Text("Product Details"),
      actions: [
        IconButton(
          icon:
              Icon(_isLiked ? Icons.favorite : Icons.favorite_border_outlined),
          onPressed: () {
            setState(() {
              _isLiked = !_isLiked;
            });
          },
        ),
      ],
    );
  }

  _bottomBar() {
    return _itemsCountOnCart > 0
        ? Container(
            padding: EdgeInsets.symmetric(
              vertical: getProportionateScreenWidth(15),
              horizontal: getProportionateScreenWidth(30),
            ),
            // height: 174,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
              boxShadow: [
                BoxShadow(
                  offset: Offset(0, -15),
                  blurRadius: 20,
                  color: Color(0xFFDADADA).withOpacity(0.15),
                )
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: getProportionateScreenHeight(1)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "$_itemsCountOnCart  items in cart",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13,
                          ),
                        ),
                        Text(
                          "₹$_itemsTotalAmountOnCart",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 150,
                      height: 40,
                      child: DefaultButton(
                        text: "View Cart",
                        press: () {
                          NavigationUtls.navigate(CartPage(), context);
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        : null;
  }

  _body() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            if (_images != null)
              Container(
                height: 250,
                alignment: Alignment.topCenter,
                margin: EdgeInsets.only(top: 20),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: ImageView(url: _imageUrl),
                ),
              ),
            SizedBox(
              height: 15,
            ),
            if (_images != null)
              Container(
                height: 50,
                margin: EdgeInsets.only(left: 5, right: 5),
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: _images == null ? 0 : _images!.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          _selectedImageIndex = index;
                          _imageUrl = kBaseUrl + _images![index]['image_url'];
                        });
                      },
                      child: Container(
                        height: 50,
                        margin: EdgeInsets.only(left: 5, right: 5),
                        padding: const EdgeInsets.all(1.0),
                        decoration: _selectedImageIndex == index
                            ? BoxDecoration(
                                border: Border.all(color: kPrimaryColor),
                                borderRadius: BorderRadius.circular(10.0),
                              )
                            : null,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: ImageView(
                                url: kBaseUrl + _images![index]['image_url'])),
                      ),
                    );
                  },
                ),
              ),
            TopRoundedContainer(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _productName,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        // Text(
                        //   '₹$_regularPrice',
                        //   maxLines: 1,
                        //   textAlign: TextAlign.left,
                        //   overflow: TextOverflow.ellipsis,
                        //   style: TextStyle(
                        //     decoration: TextDecoration.lineThrough,
                        //     color: Colors.grey,
                        //     fontWeight: FontWeight.bold,
                        //     fontSize: 16,
                        //   ),
                        // ),
                        // SizedBox(width: 5),
                        Text(
                          '₹$_salePrice',
                          maxLines: 1,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: kPriceColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Text(
                      _description,
                      maxLines: 3,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    if (_variants != null) _variantsView(),
                    SizedBox(
                      height: 20,
                    ),
                    _addToCartButtons(),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
            Divider(),
            Container(
              color: Colors.white,
              width: double.infinity,
              padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
              child: Text(
                "You may also like",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 5, right: 5),
              child: _relatedProducts(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _variantsView() {
    return Center(
      child: Container(
        height: 50,
        margin: EdgeInsets.only(left: 5, right: 5),
        child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: _variants == null ? 0 : _variants!.length,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  _isFirstLoadVariant = false;
                  _selectedVariantIndex = index;
                  _regularPrice =
                      double.parse(_variants![index]['regular_price']);
                  _salePrice = double.parse(_variants![index]['sale_price']);
                  _variantId = _variants![index]['id'];
                  _variantName = _variants![index]['name'];
                });

                updateQtyBasedOnCart();
              },
              child: _sizeWidget(
                _variants![index]['name'],
                int.parse(
                  _variants![index]['show_price'],
                ),
                index,
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _sizeWidget(String text, int isShowPrice, int index) {
    bool isSelected = true;
    if (_isFirstLoadVariant) {
      isSelected = isShowPrice == 1;
    } else {
      isSelected = _selectedVariantIndex == index;
    }

    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(left: 5, right: 5),
      decoration: BoxDecoration(
        border: Border.all(
            color: Colors.grey,
            style: !isSelected ? BorderStyle.solid : BorderStyle.none),
        borderRadius: BorderRadius.all(Radius.circular(13)),
        color: isSelected ? kPrimaryColor : Colors.white,
      ),
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            fontSize: 16,
            color: isSelected ? Colors.white : Colors.black,
          ),
        ),
      ),
    );
  }

  _relatedProducts() {
    var maxWidth = 200.0;
    var width = MediaQuery.of(context).size.width;
    var columns = (width ~/ maxWidth) + 1;
    var columnWidth = width / columns;
    //160 is the height of one grid item
    var aspectRatio = columnWidth / 250;

    return _relatedProdcuts != null
        ? GridView.builder(
            itemCount: _relatedProdcuts!.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: maxWidth,
              childAspectRatio: aspectRatio,
            ),
            itemBuilder: (BuildContext context, int index) {
              return ProductCard(
                productModel: _relatedProdcuts![index],
              );
            },
          )
        : null;
  }

  _addToCartButtons() {
    return _itemCount > 0
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () async {
                  if (_itemCount > 1) {
                    int qty = _itemCount - 1;
                    await _dbHandler.updateProductQtyOnCart(
                      _productId,
                      _variantId,
                      qty,
                    );
                    setState(() {
                      _itemCount = qty;
                    });
                    updateBottomBarDatas();
                  } else {
                    showAlertDialog(context, _productId, _variantId);
                  }
                },
                child: Text(
                  "-",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  primary: Colors.white,
                  backgroundColor: Colors.grey,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 25,
                  right: 25,
                ),
                child: Text(
                  "$_itemCount",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () async {
                  int qty = _itemCount + 1;
                  await _dbHandler.updateProductQtyOnCart(
                    _productId,
                    _variantId,
                    qty,
                  );
                  setState(() {
                    _itemCount = qty;
                  });
                  updateBottomBarDatas();
                },
                child: Text(
                  "+",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  primary: Colors.white,
                  backgroundColor: Colors.green,
                ),
              ),
              // SizedBox(
              //   width: 15,
              // ),
              // Icon(
              //   Icons.delete,
              //   color: Colors.red,
              //   size: 32,
              // ),
            ],
          )
        : Padding(
            padding: EdgeInsets.only(
              left: 10,
              right: 10,
              bottom: 10,
              top: 10,
            ),
            child: DefaultButton(
              text: "Add To Cart",
              press: () async {
                ProductModel product = new ProductModel(
                  id: _productId,
                  name: _productName,
                  regularPrice: _regularPrice,
                  salePrice: _salePrice,
                  imageUrl: _defualtImageUrl,
                  categoryId: _categoryId,
                  categoryName: _categoryName,
                  variantId: _variantId,
                  variantName: _variantName,
                  productDescription: _description,
                  quantity: 1,
                );

                int id = await _dbHandler.insertToCart(product);
                if (id > 0) {
                  print("Success");
                  showSnackBar(context, "Item added to cart");
                } else {
                  print("Error");
                }

                setState(() {
                  _itemCount = 1;
                });
                updateBottomBarDatas();
              },
            ),
          );
  }

  fetchProductDetails() async {
    String url = Urls.get_product_details + "?id=" + _productId.toString();

    RestService.makeGetRequestOpen(url, (Map response) {
      print("On success");

      if (mounted)
        setState(() {
          _productName = response['product_details']['product_name'];
          _description = response['product_details']['description'];
          _imageUrl =
              kBaseUrl + response['product_details']['default_image_url'];
          _regularPrice =
              double.parse(response['product_details']['regular_price']);
          _salePrice = double.parse(response['product_details']['sale_price']);

          _defualtImageUrl = _imageUrl;

          _categoryId = response['product_details']['category_id'];
          _categoryName = response['product_details']['category_name'];

          _variantId = response['product_details']['variant_id'];
          _variantName = response['product_details']['variant_name'];

          _variants = response['variants'];
          _images = [];
          _images!.add({
            'id': 10,
            'image_url': response['product_details']['default_image_url']
          });
          _images!.addAll(response['images']);
        });

      updateQtyBasedOnCart();
    }, (String error) {
      print("On Error");
      print(error);
    }, (bool loader) {
      print("On Loader");
      setState(() {
        _isLoading = loader;
      });
    });
  }

  updateQtyBasedOnCart() async {
    _productOnCart = await _dbHandler.getProductOnCart(_productId, _variantId);
    if (_productOnCart != null) {
      AppLogger.log('Exist in cart');
      AppLogger.log(_productOnCart.toString());

      setState(() {
        _itemCount = _productOnCart!.quantity.round();
      });
    } else {
      AppLogger.log('Not in cart');
      setState(() {
        _itemCount = 0;
      });
    }
  }

  updateBottomBarDatas() async {
    List<ProductModel> list = await _dbHandler.getAllCarts();
    double amount = 0;
    for (var item in list) {
      amount = amount + (item.quantity * item.salePrice);
    }

    setState(() {
      _itemsCountOnCart = list.length;
      _itemsTotalAmountOnCart = amount;
    });
  }

  showAlertDialog(BuildContext context, int productId, int variantId) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );
    Widget continueButton = TextButton(
      child: Text(
        "Delete Item",
        style: TextStyle(
          color: Colors.red,
        ),
      ),
      onPressed: () async {
        await _dbHandler.deleteProductFromCart(productId, variantId);
        Navigator.of(context, rootNavigator: true).pop('dialog');
        setState(() {
          _itemCount = 0;
        });
        updateBottomBarDatas();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text("Are you sure want to delete item from cart?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  fetchRelatedProducts() async {
    String url =
        Urls.get_related_products + "?productId=" + _productId.toString();

    RestService.makeGetRequestOpen(url, (Map response) {
      print("On success");

      List dataList = response['products'];

      ProductModel product;
      List<ProductModel> _tempList = [];
      for (var data in dataList) {
        product = new ProductModel(
          id: data['id'],
          name: data['product_name'],
          regularPrice: double.parse(data['regular_price']),
          salePrice: double.parse(data['sale_price']),
          imageUrl: data['default_image_url'],
          categoryId: data['category_id'],
          categoryName: data['category_name'],
          variantId: data['variant_id'],
          variantName: data['variant_name'],
          productDescription: data['description'],
          quantity: 0,
        );

        _tempList.add(product);
      }

      if (mounted)
        setState(() {
          _relatedProdcuts = _tempList;
        });
    }, (String error) {
      print("On Error");
      print(error);
    }, (bool loader) {
      print("On Loader");
    });
  }
}
