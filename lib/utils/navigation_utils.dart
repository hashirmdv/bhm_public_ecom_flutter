import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NavigationUtls {
  static void navigate(Widget page, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => page),
    );
    // Navigator.of(context)
    //     .push(new MaterialPageRoute(builder: (BuildContext context) => page));
  }

  static void navigateAndFinish(Widget page, BuildContext context) {
    Navigator.of(context).pushReplacement(
        new MaterialPageRoute(builder: (BuildContext context) => page));
  }

  static void finish(BuildContext context, {String? result}) {
    if (Navigator.canPop(context)) {
      if (result != null && result.trim().length > 0) {
        Navigator.pop(context, result);
      } else {
        Navigator.pop(context);
      }
    } else {
      SystemNavigator.pop();
    }
  }

  static void navigateAndFinishAllPages(Widget page, BuildContext context) {
    Navigator.of(context).pushAndRemoveUntil(
      new MaterialPageRoute(builder: (BuildContext context) => page),
      ModalRoute.withName('/'),
    );
  }

  static navigateAndWaitForResult(Widget page, BuildContext context) async {
    return await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => page),
    );
  }
}
