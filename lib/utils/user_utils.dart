import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/pages/sign_in_page.dart';
import 'package:single_vendor_ecom/utils/navigation_utils.dart';

class UserUtils {
  static Future<String?> getProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? profile = prefs.getString(kSharedPrefUserDetailsKey);
    print("User profile: " + (profile != null ? profile.toString() : ""));
    return profile;
  }

  static void logout(BuildContext context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();

    NavigationUtls.navigateAndFinishAllPages(SignInPage(), context);
  }

  static Future<String?> getLocation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? location = prefs.getString(kSharedPrefLocationKey);
    print("Location: " + (location != null ? location.toString() : ""));
    return location;
  }
}
