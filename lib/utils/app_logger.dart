import 'package:single_vendor_ecom/cons/app_const.dart';

class AppLogger {
  static log(String log) {
    print("${kAppName} : $log");
  }
}
