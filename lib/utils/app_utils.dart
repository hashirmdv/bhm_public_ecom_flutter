import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AppUtils {
  static getValueFromMap(Map map, String key) {
    if (map == null)
      return "";
    else if (key == null)
      return "";
    else
      return map[key];
  }

  static int parseIntFromString(String str) {
    return str != null && str.length > 0 && isDigit(str) ? int.parse(str) : 0;
  }

  static bool parseBoolFromString(String str) {
    return str != null && str.length > 0 && str.toLowerCase() == 'true'
        ? true
        : false;
  }

  ///
  /// Checks if the given string [s] is a digit.
  ///
  /// Will return false if the given string [s] is empty.
  ///
  static bool isDigit(String s) {
    if (s.isEmpty) {
      return false;
    }
    if (s.length > 1) {
      for (var r in s.runes) {
        if (r ^ 0x30 > 9) {
          return false;
        }
      }
      return true;
    } else {
      return s.runes.first ^ 0x30 <= 9;
    }
  }

  static bool isValidEmail(String str) {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(str);
  }

  static showSimpleAlert(
      {required BuildContext context,
      title,
      Color? color,
      body,
      Map<String, VoidCallback>? actionList}) {
    List<Widget> actionWidgets = [];
    actionList?.forEach((key, func) {
      actionWidgets.add(new TextButton(
        onPressed: () {
          Navigator.pop(context);
          func();
        },
        child: new Text(key.toString()),
      ));
    });

    AlertDialog alert = new AlertDialog(
      title: new Text(
        "$title",
        style: TextStyle(
          fontWeight: FontWeight.w600,
          color: color,
        ),
      ),
      actions: actionWidgets,
      content: new Text(
        "$body",
        style: Theme.of(context).textTheme.bodyText2,
      ),
    );
    showDialog(context: context, builder: (BuildContext context) => alert);
  }

  static String printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  static void launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
