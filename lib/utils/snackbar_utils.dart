import 'package:flutter/material.dart';

// page without scaffold or another context
showSnackBar(context, text, {label}) {
  final snackBar = SnackBar(
    content: Text(text),
    duration: Duration(seconds: 2),
    action: SnackBarAction(
      label: label ?? "",
      onPressed: () {},
    ),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

showSimpleSnackBar(context, text, [key]) {
  final snackBar = SnackBar(
    content: Text(text),
    duration: Duration(seconds: 2),
  );
  key == null
      ? ScaffoldMessenger.of(context).showSnackBar(snackBar)
      : key.currentState.showSnackBar(snackBar);
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

// page snackbar
snackBar(context, scaffolKey, text, label) {
  final snackBar = SnackBar(
    duration: Duration(seconds: 2),
    content: Text(text),
    action: SnackBarAction(
      label: label,
      onPressed: () {},
    ),
  );
  scaffolKey.currentState.showSnackBar(snackBar);
}

snackBarWithFloating(context, scaffolKey, text) {
  final snackBar = SnackBar(
    behavior: SnackBarBehavior.fixed,
    duration: Duration(seconds: 2),
    content: Text(text),
  );
  scaffolKey.currentState.showSnackBar(snackBar);
}
