import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

const String kAppName = "BHM";

const String kBaseUrl = "https://demo.bismillahhalal.com/";
const String kApiBaseUrl = kBaseUrl + "PublicAppApi/";

const String kSharedPrefUserDetailsKey = "user_details";
const String kSharedPrefUserTokenKey = "user_token";
const String kSharedPrefLocationKey = "location";
const String kSharedPrefLoginSkippedKey = "is_login_skipped";

const kInitialPosition = LatLng(-33.8567844, 151.213108);
