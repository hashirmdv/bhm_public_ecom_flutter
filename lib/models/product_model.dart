class ProductModel {
  ProductModel({
    required this.id,
    required this.name,
    required this.regularPrice,
    required this.salePrice,
    required this.imageUrl,
    required this.categoryId,
    required this.categoryName,
    required this.variantId,
    required this.variantName,
    required this.productDescription,
    required this.quantity,
  });

  int id;
  String name;
  double regularPrice;
  double salePrice;
  String imageUrl;

  int categoryId;
  String categoryName;
  int variantId;
  String variantName;
  String productDescription;

  double quantity;

  factory ProductModel.fromCartMap(Map<String, dynamic> res) {
    return ProductModel(
      id: res['product_id'],
      name: res['product_name'],
      regularPrice: res['regular_price'],
      salePrice: res['sale_price'],
      imageUrl: res['image_url'],
      categoryId: res['category_id'],
      categoryName: res['category_name'],
      variantId: res['variant_id'],
      variantName: res['variant_name'],
      productDescription: res['description'],
      quantity: res['quantity'],
    );
  }

  Map<String, Object?> toCartMap() {
    return {
      'product_id': id,
      'product_name': name,
      'regular_price': regularPrice,
      'sale_price': salePrice,
      'image_url': imageUrl,
      'category_id': categoryId,
      'category_name': categoryName,
      'variant_id': variantId,
      'variant_name': variantName,
      'description': productDescription,
      'quantity': quantity,
    };
  }

  @override
  String toString() {
    return {
      'product_id': id,
      'product_name': name,
      'regular_price': regularPrice,
      'sale_price': salePrice,
      'image_url': imageUrl,
      'category_id': categoryId,
      'category_name': categoryName,
      'variant_id': variantId,
      'variant_name': variantName,
      'description': productDescription,
      'quantity': quantity,
    }.toString();
  }
}
