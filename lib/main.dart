import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:single_vendor_ecom/cons/app_const.dart';
import 'package:single_vendor_ecom/pages/splash_page.dart';
import 'package:single_vendor_ecom/values/theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setPreferredOrientations([
    //   DeviceOrientation.portraitUp,
    //   DeviceOrientation.portraitDown,
    // ]);
    return MaterialApp(
      title: kAppName,
      debugShowCheckedModeBanner: false,
      theme: theme(),
      home: SplashPage(),
    );
  }
}
